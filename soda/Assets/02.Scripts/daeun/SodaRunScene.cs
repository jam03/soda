using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

public class SodaRunScene : MonoBehaviourPunCallbacks {
    [SerializeField] private FollowCam cam;
    [SerializeField] private ObjectGenerator objectGenerator;
    [SerializeField] private Transform finishLine;
    [SerializeField] private Text startMsg;

    private List<PhotonView> players = new List<PhotonView>();
    private Runner player;
    private bool isInit = false;

    private int playerCurrentStep = 0;

    void Start() {
    }

    public void GameStart() {

        ShowStartMsg();
    }

    private async void ShowStartMsg() {
        startMsg.gameObject.SetActive(true);
        startMsg.text = "3";
        await Task.Delay(TimeSpan.FromSeconds(1f));
        startMsg.text = "2";
        await Task.Delay(TimeSpan.FromSeconds(1f));
        startMsg.text = "1";
        await Task.Delay(TimeSpan.FromSeconds(1f));
        startMsg.text = "Start";
        SodaRunGameManager.State = SodaRunGameManager.GameState.START;
        await Task.Delay(TimeSpan.FromSeconds(0.2f));
        startMsg.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        if (isInit == false) return;
        if (SodaRunGameManager.State == SodaRunGameManager.GameState.READY) return;
        
        if (Input.GetKey(KeyCode.Space)) {
            player.Move();
        } else {
            player.Stop();
        }

        if ((int) player.transform.position.x / 10 > playerCurrentStep) {
            playerCurrentStep = (int) player.transform.position.x / 10;
            objectGenerator.Generate(player.transform.position.x);
        }

        if (player.transform.position.x > finishLine.position.x) {
        }
    }
    

    public override void OnJoinedRoom() {
        player = PhotonNetwork.Instantiate( "RunnerCharacter", new Vector3( -5.5f, Random.Range(-3f,3f), 0 ), Quaternion.identity, 0, null ).GetComponent<Runner>();
        isInit = true;
        
        cam.SetFollowing(true, player.transform);
        Debug.Log($"PlayerCount: {PhotonNetwork.CurrentRoom.PlayerCount}");
        if (PhotonNetwork.CurrentRoom.PlayerCount >= 2) {
            player.Broadcast();
        }
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        foreach (var pt in propertiesThatChanged) {
            var pos = pt.Value as string;
            if (float.TryParse(pos, out float result) == false) {
                Debug.LogError($"invalid {pt.Key} {pt.Value}");
                return;
            }
            SodaRunGameManager.Instance.RefreshPlayersPosition(pt.Key as string,  float.Parse(pos, NumberStyles.Float));
        }
    }
}
