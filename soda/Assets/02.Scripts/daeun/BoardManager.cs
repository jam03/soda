using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour {
    [SerializeField] private Text[] texts;
    
    public void Show(Dictionary<string, float> dict) {
        var items = from pair in dict
            orderby pair.Value ascending
            select pair;

        int idx = 0;
        foreach (var pair in items) {
            texts[idx].gameObject.SetActive(true);
            texts[idx].text = $"{idx+1}: {pair.Key}";
            idx++;
        }
    }
}
