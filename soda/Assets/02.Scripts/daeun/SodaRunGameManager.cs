
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;

public class SodaRunGameManager : MonoBehaviour {
	private static SodaRunGameManager _instance;
	public static SodaRunGameManager Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(SodaRunGameManager)) as SodaRunGameManager;
			}

			return _instance;
		}
	}

	[SerializeField] private SodaRunScene playScene;
	[SerializeField] private BoardManager boardManager;
	
	public enum GameState {
		READY,
		START,
		PLAYING,
		GAME_OVER
	}
	
	public static GameState State { get; set; } = GameState.READY;

	public void GameStart() {
		playScene.GameStart();
	}

	public void UpdatePosition(float pos) {
		Hashtable hashtable = new Hashtable();
		hashtable.Add($"position_{PhotonNetwork.LocalPlayer.ActorNumber}", pos.ToString());
		PhotonNetwork.CurrentRoom.SetCustomProperties(hashtable);
	}

	public void RefreshPlayersPosition(string p, float pos) {
		if (playerPosition.ContainsKey(p) == false) {
			playerPosition.Add(p, pos);
		}

		playerPosition[p] = pos;
		
		boardManager.Show(playerPosition);
	}
	
	Dictionary<string, float> playerPosition = new Dictionary<string, float>();
	
}
