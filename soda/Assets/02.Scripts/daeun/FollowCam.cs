using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowCam : MonoBehaviour {
    private Camera cam;
    private Transform target;
    

    private bool enableFollowing = false;
    private bool isFollowing = false;
    
    void Start() {
        cam = GetComponent<Camera>();
    }

    public void SetFollowing(bool isActive, Transform t) {
        enableFollowing = isActive;
        isFollowing = false;
        target = t;
    }

    void Update() {
        if (enableFollowing == false) return;

        var position = cam.transform.position;
        if (!isFollowing && target.position.x < position.x) {
            return;
        }

        isFollowing = true;
        
        position.x = target.position.x;
        transform.position = position;
    }
}
