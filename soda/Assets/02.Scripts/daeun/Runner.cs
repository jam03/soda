using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Runner : MonoBehaviour, IPunObservable {
    [SerializeField] private Animator animator;
    [SerializeField] private float speed = 5f;
    [SerializeField] private PhotonView _photonView;
    private Transform _transform;
    
    public enum State {
        IDLE,
        RUN
    }
    public State CurrentState { get; private set; }
    
    void Start() {
        _transform = transform;
    }

    public void Broadcast() {
        _photonView.RPC("StartGame", RpcTarget.All);
    }

    [PunRPC]
    public void StartGame() {
        SodaRunGameManager.Instance.GameStart();
        Debug.Log($"STARTGAME {PhotonNetwork.CurrentRoom.PlayerCount}");
    }

    void Update()
    {
        SodaRunGameManager.Instance.UpdatePosition(_transform.localPosition.x);
        
    }

    public void Move() {
        var position = _transform.localPosition;
        position.x += speed * Time.deltaTime;
        _transform.localPosition = position;

        if (CurrentState == State.IDLE) {
            CurrentState = State.RUN;
            animator.Play("run");
        }
        
    }

    public void Stop() {
        if (CurrentState == State.RUN) {
            animator.Play("idle");
            CurrentState = State.IDLE;
        }
    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        // throw new System.NotImplementedException();
    }
}
