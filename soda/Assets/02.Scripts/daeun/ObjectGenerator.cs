using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour {
    [SerializeField] private GameObject originObject;

    private List<GameObject> objectList = new List<GameObject>();
    
    Vector2 DefaultPosition = new Vector2(15f, 2.5f);
    void Start()
    {
        Generate();
    }

    public void Generate(float x = 0f) {
        var position = DefaultPosition;
        position.x += x + Random.Range(-1f, 1f);
        position.y += Random.Range(-1f, 2.5f);

        var obj = Instantiate(originObject, position, Quaternion.identity, transform);
        objectList.Add(obj);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
