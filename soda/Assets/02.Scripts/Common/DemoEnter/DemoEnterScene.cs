using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoEnterScene : MonoBehaviour{
    public void OnClickEnterScene(string sceneName){
        Debug.Log($"Scene Change{sceneName}");
        SceneManager.LoadScene(sceneName);
    }
}
