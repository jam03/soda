using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
/*
 * 이 시점에서 Launch 씬을 저장할 수 있고 PhotonServerSettings
 * (유니티 메뉴 Window/Photon Unity Networking/Highlight Photon Server Settings에서 선택)을 오픈하여 PUN 로깅을 "Full" 로 설정하도록 합니다:
 */

namespace Soda03.Base {
    public enum AutoConnectType {
        OnlyConnect,AutoJoinRoom
    }
    public class Launcher : MonoBehaviourPunCallbacks {
        [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        [SerializeField]
        private byte maxPlayersPerRoom = 4;
        [SerializeField] private AutoConnectType autoType = AutoConnectType.AutoJoinRoom;
        string gameVersion = "1";
        void Awake()
        {
            // 이것은 마스터 클라이언트에서 PhotonNetwork.LoadLevel()을 사용할 수 있고 같은 방에 있는 모든 클라이언트가 자동으로 레벨을 동기화할 수 있는지 확인합니다.
            PhotonNetwork.AutomaticallySyncScene = true;
        }
        void Start()
        {
            Connect();
        }
        
        public void Connect()
        {
            // 연결되었는지 여부를 확인하고 연결되어 있으면 가입하고 그렇지 않으면 서버에 연결을 시작합니다.
            if (PhotonNetwork.IsConnected)
            {
                // 이 시점에서 랜덤 룸에 참여를 시도하는 데 필요합니다. 실패하면 OnJoinRandomFailed()에서 알림을 받고 하나를 만듭니다.
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                // 우리는 가장 먼저 Photon Online Server에 연결해야 합니다.
                PhotonNetwork.GameVersion = gameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
        }
        
        
        public override void OnConnectedToMaster()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnConnectedToMaster() was called by PUN");
            if (autoType == AutoConnectType.AutoJoinRoom) {
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnConnected() {
            base.OnConnected();
            
            if (autoType == AutoConnectType.AutoJoinRoom) {
                PhotonNetwork.JoinRandomRoom();
            }
        }


        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reason {0}", cause);
        }
        
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        }
    }
}

