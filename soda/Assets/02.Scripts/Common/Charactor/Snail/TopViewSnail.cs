using System;
using UnityEngine;
using System.Collections;
using Photon.Pun;

namespace hw {
    
public class TopViewSnail : MonoBehaviour , IPunObservable {
    [SerializeField] Animator m_Animator;
    Rigidbody2D m_Body;
    PhotonView m_PhotonView;
    [SerializeField] float charactorScale = 0.75f;
    PhotonTransformView m_TransformView;
    private Vector3 preVelocity;
    bool m_IsGrounded;
    private float speed = 10f;

    void Awake() {
        m_Body = GetComponent<Rigidbody2D>();
        m_PhotonView = GetComponent<PhotonView>();
        m_TransformView = GetComponent<PhotonTransformView>();
        transform.localScale = new Vector3( -charactorScale, charactorScale, 1 );
    }

    void Update() {
        UpdateIsGrounded();
        UpdateIsRunning();
        UpdateFacingDirection();
    }



    void UpdateFacingDirection() {
        if( m_Body.velocity.x > 0.01f )
        {
            transform.localScale = new Vector3( -charactorScale, charactorScale, 1 );
        }
        else if( m_Body.velocity.x < -0.01f )
        {
            transform.localScale = new Vector3( charactorScale, charactorScale, 1 );
        }
    }

    void FixedUpdate() {
        if (m_PhotonView.IsMine) {
            UpdateMovement();
        }
    }
    void UpdateMovement() {
        var dir = Vector3.zero;
        if (Input.GetKey(KeyCode.UpArrow)) dir += Vector3.up;
        if (Input.GetKey(KeyCode.DownArrow)) dir += Vector3.down;
        if (Input.GetKey(KeyCode.LeftArrow)) dir += Vector3.left;
        if (Input.GetKey(KeyCode.RightArrow)) dir += Vector3.right;

        dir = dir.normalized;
        m_Body.AddForce(dir.normalized * speed);
        preVelocity = m_Body.velocity;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (m_PhotonView.IsMine) {
            var wall = other.transform.GetComponent<WallObstacle>();
            if (wall != null) {
                m_Body.AddForce(wall.GetReflectVector(preVelocity) * 20f);
            }
        }
    }

    [PunRPC]
    void DoCol()
    {
    }


    void UpdateIsRunning()
    {
    }

    void UpdateIsGrounded() {
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting)
        {
            stream.SendNext(m_Body.position);
            stream.SendNext(m_Body.rotation);
            stream.SendNext(m_Body.velocity);
        }
        else
        {
            m_Body.position = (Vector3) stream.ReceiveNext();
            m_Body.rotation = (float) stream.ReceiveNext();
            m_Body.velocity = (Vector3) stream.ReceiveNext();

            float lag = Mathf.Abs((float) (PhotonNetwork.Time - info.timestamp));
            m_Body.position += m_Body.velocity * lag;
        }

    }
}

}