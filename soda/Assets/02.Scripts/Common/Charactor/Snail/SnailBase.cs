
using UnityEngine;

public class SnailBase : MonoBehaviour{
    protected enum SNAIL_STATE{
        DEATH_Snail,FALL_Snail,HIDEin_Snail,HIDEloop_Snail,HIDEout_Snail,HIT_Snail,IDLE_Snail,
        JUMPend_Snail,JUMPstart_Snail,LOOK_Snail,MOVE_Snail,SPEEDend_Snail,SPEEDloop_Snail,SPEEDstart_Snail,VICTORY_Snail
    }
    [SerializeField] private Animator animator;

    private void Update(){
        for (int i = 0; i < 10; i++){
            if (Input.GetKeyDown((KeyCode) (48 + i))){
                PlayAnim((SNAIL_STATE)i);
            }
        }
    }

    protected void PlayAnim(SNAIL_STATE state){
        animator.StopPlayback();
        animator.Play(state.ToString());
    }
    
}
