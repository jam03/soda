using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChangeButton : MonoBehaviour {
    [SerializeField] private Button button;
    [SerializeField] private string ChangeSceneName = "DemoEnter";
    private void Start() {
        if (button == null) {
            button = GetComponent<Button>();
            Debug.Assert(button!= null, "button == null");
            button.onClick.AddListener(OnClickButton);
        }
    }

    public void OnClickButton() {
        SceneManager.LoadScene("DemoEnter");
    }
    
    
}
