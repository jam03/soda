using System;
using System.Collections;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class GameManagerSH : MonoBehaviour {
    [SerializeField] private ObstacleGenerator _objectGenerator;
    [SerializeField] private TextMeshProUGUI startText;

    public static Action OnGameStart;
    
    public void Start() {
        StartCoroutine(CoStart());
    }

    private IEnumerator CoStart() {
        yield return new WaitUntil(() => PhotonNetwork.InRoom);
        startText.gameObject.SetActive(true);
        startText.text = 3.ToString();
        yield return new WaitForSeconds(1f);
        startText.text = 2.ToString();
        yield return new WaitForSeconds(1f);
        startText.text = 1.ToString();
        yield return new WaitForSeconds(1f);
        startText.gameObject.SetActive(false);
        
        OnGameStart?.Invoke();
    }
}