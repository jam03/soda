using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour {
    
    [SerializeField] private float jumpPower = 1f;
    [SerializeField] private int jumpAccel = 5;

    private float jumpBtnPressTime;
    
    private bool isJumping = false;
    private bool canMove = false;
    
    private Rigidbody2D rigidbody2D;

    void Start() {
        rigidbody2D = GetComponent<Rigidbody2D>();

        GameManagerSH.OnGameStart += BeMovable;
    }

    private void BeMovable() {
        canMove = true;
    }
    
    void Update() {
        if (!canMove) return;
        
        if (Input.GetButton("Jump")) {
            isJumping = true;
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.AddForce(Vector2.up * jumpPower * ((jumpBtnPressTime * jumpAccel) + 1f), ForceMode2D.Impulse);

            jumpBtnPressTime += Time.deltaTime;
        } else {
            isJumping = false;
            jumpBtnPressTime = 0f;
        }
    }
}
