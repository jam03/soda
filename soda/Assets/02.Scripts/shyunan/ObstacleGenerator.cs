using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Random = System.Random;

public class ObstacleGenerator : MonoBehaviour {
    [SerializeField] private GameObject[] goObstacles;
    
    public float term = 7f;
    private Random _random = new Random();

    public void Awake() {
        GameManagerSH.OnGameStart += StartGenerate;
    }

    private void StartGenerate() {
        StartCoroutine(CoGenerate());
    }

    private IEnumerator CoGenerate() {
        while (true) {
            var randIndex = _random.Next(0, goObstacles.Length);
            var goObstacle = Instantiate(goObstacles[randIndex], transform);

            var randDir = _random.Next(0, 2);
            randDir = randDir == 0 ? 1 : -1;
            var deltaY = randDir * (float)_random.NextDouble() * Camera.main.orthographicSize;
            goObstacle.transform.position = transform.position + Vector3.up * deltaY;

            yield return new WaitForSeconds(term);
        }
    }
}