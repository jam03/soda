using System;
using Photon.Pun;
using UnityEngine;

public class PlayerCreater : MonoBehaviourPunCallbacks {
    [SerializeField] private GameObject player;  

    public override void OnJoinedRoom() {
        player = PhotonNetwork.Instantiate(player.name, Vector2.zero, Quaternion.identity, 0);;
    }
}