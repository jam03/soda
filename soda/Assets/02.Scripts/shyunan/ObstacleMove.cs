using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMove : MonoBehaviour {
    private Transform myTransform;
    
    void Start() {
        myTransform = transform;
    }
    void Update() {
        var position = myTransform.position;
        var x = position.x - Time.deltaTime;
        position = new Vector3(x, position.y, position.z);
        myTransform.position = position;
    }
}
