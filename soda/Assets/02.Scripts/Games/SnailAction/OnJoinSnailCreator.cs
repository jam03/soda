using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class OnJoinSnailCreator : MonoBehaviourPunCallbacks {
    public Rect SpawnRect = new Rect(new Vector2(0, 0), new Vector2(14, 7));
    public GameObject snailPlayer; // set in inspector


    public override void OnJoinedRoom() {
        if (snailPlayer != null) {
            PhotonNetwork.Instantiate(snailPlayer.name, new Vector3(
                Random.Range(-SpawnRect.width / 2, SpawnRect.width / 2),
                Random.Range(-SpawnRect.height / 2, SpawnRect.height / 2),
                0), Quaternion.identity, 0);
        }
    }
}
