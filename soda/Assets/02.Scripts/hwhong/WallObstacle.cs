using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hw {
    enum WALL_DIR {
        TOP,LEFT,RIGHT,BOTTOM,ROT45,ROT135, NONE
    }
    public class WallObstacle : MonoBehaviour {
        [SerializeField] private WALL_DIR dir;

        public Vector3 GetReflectVector(Vector3 vec) {
            switch (dir) {
                case WALL_DIR.TOP: case WALL_DIR.BOTTOM: 
                    return new Vector3(vec.x,-vec.y,vec.z);
                case WALL_DIR.LEFT: case WALL_DIR.RIGHT: 
                    return new Vector3(-vec.x,vec.y,vec.z);
                case WALL_DIR.ROT45:
                    return new Vector3(-vec.y,-vec.x,vec.z);
                case WALL_DIR.ROT135:
                    return new Vector3(vec.y,vec.x,vec.z);
            }

            return Vector3.zero;
        }
    }
}

